# The Old Reddit Chrome Extension

Chrome Extension that redirects all reddit.com URLs to old.reddit.com.

## Installation

 1) Visit chrome://extensions (via omnibox or menu -> Tools -> Extensions).
 2) Enable Developer mode by ticking the checkbox in the upper-right corner.
 3) Click on the "Load unpacked extension..." button.
 4) Select the extension directory.